class TrackSerializer(serializers.ModelSerializer):

    id = IntegerField()

    class Meta:
        model = Track
        fields = ('id', 'name', 'artist')


class PlaylistSerializer(serializers.ModelSerializer):

    tracks = TrackSerializer(many=True)

    class Meta:
        model = Playlist
        fields = ('id', 'name', 'tracks')

    def create(self, validated_data):
        tracks = validated_data.pop('tracks')
        playlist = Playlist.objects.create(**validated_data)
        playlist.tracks = [track['id'] for track in tracks]
        return playlist

    def update(self, instance, validated_data):
        tracks = validated_data.pop('tracks')
        instance.tracks = [track['id'] for track in tracks]
        return instance