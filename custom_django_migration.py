@transaction.atomic
def set_urls(apps, schema_editor):
   Channel = apps.get_model("channel", "Channel")
   for obj in Channel.objects.all():
      obj.url_name = slugify(unicode(obj.title))
      if Channel.objects.filter(url_name=obj.url_name).exists():
         obj.url_name += str(uuid4())[:4]
      obj.save()


class Migration(migrations.Migration):

    dependencies = [
        ('channel', '0003_auto_20150220_1752'),
    ]

    operations = [
        migrations.AddField(
            model_name='channel',
            name='url_name',
            field=models.SlugField(null=True, max_length=255, blank=True, unique=True, verbose_name='Channel URL'),
            preserve_default=True,
        ),
        migrations.RunPython(set_urls),
    ]