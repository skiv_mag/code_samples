class ChannelQuerySet(QuerySet):
   def search(self, keyword, date=None):
      return self.filter(
            Q(title__icontains=keyword) | Q(description__icontains=keyword)
      )

   def latest_by_article(self):
      return self.order_by('-articles__date_published')

   def home_featured(self):
      return self.exclude(home_featured=None).order_by('home_featured__order')

   def featured(self):
      return self.exclude(featured=None).order_by('featured__order')

   def common(self):
      return self.filter(featured=None)

   def exclude_articles(self, excluded_articles):
      return self.exclude(articles__in=excluded_articles)

class BaseChannel(Sortable, BaseContent, UrlGenerationMixin, SocialPropertyMixin):
   title = CharField(_("title"), max_length=255)
   description = TextField(_("description"))
   logo = FileField(_("logo"), upload_to="channels", blank=True, null=True)
   wide_logo = FileField(_("wide logo"), upload_to="channels", blank=True, null=True)
   cover = FileField(_("cover"), upload_to="channels", blank=True, null=True)
   url_name = SlugField(_("channel URL"), max_length=255, unique=True, blank=True, null=True)

   objects = ChannelQuerySet.as_manager()

#Topic.objects.common().latest_by_article()