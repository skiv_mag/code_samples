class CRUDMixin(object):

    """ Basic CRUD mixin for flask-sqlalchemy models
    """
    
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()
    
    def __repr__(self):
        return "<%s:%s>" % (self.__class__.__name__,
                            self.id)

    def save(self, commit=True):
        db.session.add(self)
        commit and db.session.commit()
        return self
    
    def delete(self, commit=True):
        db.session.delete(self)
        commit and db.session.commit()

    @declared_attr
    def id(cls):
        return db.Column(db.Integer, primary_key=True)

    @declared_attr
    def created_at(cls):
        return db.Column(db.DateTime, default=datetime.utcnow)

    @classmethod
    def get(cls, id):
        return cls.query.get(id)

    @classmethod
    def create(cls, commit=True, **kwargs):
        return cls(**kwargs).save(commit)

    def update(self, commit=True, **kwargs):
        return self._setattrs(**kwargs).save(commit)